define([],function(){
	'use strict';

	return function($stateProvider,$urlRouterProvider){

		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('home',{
				url: '/',
				templateUrl : 'views/home/home.html',
				controller : 'HomeController',
				controllerAs : 'Home'
			})
			.state('books',{
				url: '/books',
				templateUrl : 'views/books/home.html',
				controller : 'BooksController',
				controllerAs : 'Books'
			});

	}


});