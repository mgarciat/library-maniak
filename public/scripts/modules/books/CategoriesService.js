define([],
function(){
	'use strict';
	return function CategoriesService($resource){
		return $resource('http://localhost:8888/categories/:verb',
	   		{ },
		   	{
		       'get': {
		           method: 'GET',
		           params: { verb : 'get' },
		           isArray: false 
		       },
		       'save' : {
		       		method : 'POST',
		       		params : {verb : 'create' }
		       }
			});
	}
});