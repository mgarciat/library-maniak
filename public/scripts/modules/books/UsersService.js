define([],
function(){
	'use strict';
	return function UsersService($resource){
		return $resource('http://localhost:8888/users/:verb',
	   		{ },
		   	{
		       'get': {
		           method: 'GET',
		           params: { verb : 'get' },
		           isArray: false 
		       },
		       'save' : {
		       		method : 'POST',
		       		params : {verb : 'create' }
		       }
			});
	}
});