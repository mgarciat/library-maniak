define([],
function(){
	'use strict';
	return function BooksService($resource){
		return $resource('http://localhost:8888/books/:verb/:id',
	   		{ },
		   	{
		       'get': {
		           method: 'GET',
		           params: { verb : 'get' },
		           isArray: false 
		       },
		       'save' : {
		       		method : 'POST',
		       		params : {verb : 'create' }
		       },
		       'remove' : {
		       		method : 'DELETE',
		       		params : { verb : 'delete', id : 'id' }
		       },
		       'update' : {
		       		method : 'PUT',
		       		params : { verb : 'update' }
		       }
			});
	}
});