define(['angular',
	'angular-resource',
	'scripts/modules/books/BooksController',
	'scripts/modules/books/BooksService',
	'scripts/modules/books/CategoriesService',
	'scripts/modules/books/UsersService'

	],function(angular,
		ngResource,
		BooksController,
		BooksService,
		CategoriesService,
		UsersService
		){
		'use strict';

		return angular.module('app.books',['ngResource','ui.bootstrap'])
		.service('BooksService',['$resource',BooksService])
		.service('CategoriesService',['$resource',CategoriesService])
		.service('UsersService',['$resource',UsersService])
		.controller('BooksController',[
			'$scope',
			'$window',
			'filterFilter',
			'$log',
			'$uibModal',
			'BooksService',
			'CategoriesService',
			'UsersService',
			BooksController]);

	});