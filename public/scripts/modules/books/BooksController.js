define(['lodash'],function(_){
	'use strict';

	return function BooksController($scope,$window,filterFilter,$log,$uibModal,BooksService,CategoriesService,UsersService) {
		var vm = this,
		NOT_BLANK_MSG = 'You must fill all the fields please.',
		DANGER_WARN_TYPE = 'danger';

		vm.currentPage = 1;
		vm.itemsPerPage = 10;
		vm.totalPages = 0;
		vm.alerts = [];
		vm.filteredBooks = {};
		vm.list = [];
		vm.categories = [];
		vm.users = [];
		vm.modal = null;
		vm.modalDelete = null;
		vm.new = {};
		vm.AVAILABLETAG = "AVAILABLE";
		vm.UNAVAILABLETAG = "UNAVAILABLE";

		$log.debug('Hello Books!');

		vm.init = function(){

			BooksService.get(function(data){
				vm.list = data.books;
				vm.setDefaultTotalPages();
				vm.resetFilters();
			});

			CategoriesService.get(function(data){
				vm.categories = data.categories;
			});

			UsersService.get(function(data){
				vm.users = data.members;
			});


		};

		vm.init();

		vm.decrementPage = function(){
			vm.currentPage--;
		};

		vm.incrementPage = function(){
			vm.currentPage++;
		};

		vm.calculateOffset = function(){
			return ( vm.currentPage -1 ) * vm.itemsPerPage;
		};

		vm.setDefaultTotalPages = function(){
			vm.totalPages = Math.ceil(vm.list.length / vm.itemsPerPage);
		};

		vm.setCurrentPage = function(n){
			vm.currentPage = n
		};

		vm.resetFilters = function(){
			vm.filters = {};
		};

		vm.range = function (min, max, step) {
			step = step || 1;
			var input = [];
			for (var i = min; i <= max; i += step) {
				input.push(i);
			}
			return input;
		};

		vm.addAlert = function(alert){
			$window.scrollTo(0, 0);
			vm.alerts.push(alert);
		};

		vm.removeAlerts = function(){
			vm.alerts = [];
		};

		vm.dismiss = function(){
			if(vm.modal){
				vm.modal.dismiss('cancel');
				vm.new = {};
				vm.modal = null;

			}
			if(vm.modalDelete){
				vm.modalDelete.dismiss('cancel');
			}
			vm.removeAlerts();
		};

		vm.reload = function(){
			location.reload();
		};

		vm.addNew = function(){
			vm.modal = $uibModal.open({
				animation : true,
				templateUrl : '/views/books/modalnew.html',
				size: 'lg',
				controller : function(){
					return vm;

				},
				controllerAs : 'Books',
				bindToController : 'true'
			});
		};

	vm.save = function(book){
		if(!book.name || book.name.trim() === '' || !book.author || book.author.trim() === '' ||
				!book.published_date || book.published_date.trim() === '' || !book.category){
				vm.removeAlerts();
			vm.addAlert({ type : DANGER_WARN_TYPE, msg : NOT_BLANK_MSG });
			return;
		}else{
			if(book.id){
				BooksService.update({book : book},function(results){
					if(results.err){
						console.log(results.err);
					}else{
						vm.dismiss();
						vm.reload();
					}

				});
			}else{
				BooksService.save({book : book},function(results){
					if(results.err){
						console.log(err);
					}else{
						vm.dismiss();
						vm.reload();
					}

				});
			}
		}
	};

	vm.edit = function(book){
		vm.new = _.cloneDeep(book);
		var filterCatTmp = _.filter(vm.categories,function(category){
			return category.category_name === vm.new.category_name;
		});
		vm.new.category = filterCatTmp[0];
		vm.modal = $uibModal.open({
			animation : true,
			templateUrl : '/views/books/modalnew.html',
			size: 'lg',
			controller : function(){
				return vm;

			},
			controllerAs : 'Books',
			bindToController : 'true'
		});
	}

	vm.formatLabel = function(model){
		if(model){
			for (var i=0; i< vm.categories.length; i++) {
				if (model.id === vm.categories[i].id) {
					if(!vm.modal){
						vm.filters.category_name = vm.categories[i].category_name;
					}
					return vm.categories[i].category_name;
				}
			}
		}
	};

	vm.delete = function(book){
		vm.deleteBook = book;
		vm.modalDelete = $uibModal.open({
			animation : true,
			templateUrl : '/views/books/modaldelete.html',
			controller : ['BooksService',function(BooksService){
				var viewdelete = this;
				viewdelete.confirmDelete = function(){
					BooksService.remove({ id : vm.deleteBook.id }, function(){
						vm.dismiss();
						vm.reload();
					});
				};
				viewdelete.dismiss = vm.dismiss;
			}],
			controllerAs : 'Books',
			bindToController : 'true'
		});

	};

	$scope.$watchGroup(['Books.filters.name','Books.filters.author','Books.filters.category'],function(newVal,oldVal){

		if((typeof(newVal[0]) !== 'undefined' && newVal[0] !== '' ) || 
			(typeof(newVal[1]) !== 'undefined' && newVal[1] !== '') ||
			(typeof(newVal[2]) !== 'undefined' && newVal[2] !== ''))
		{
			vm.filteredBooks = filterFilter(vm.list,vm.filters);
			vm.totalPages = Math.ceil(vm.filteredBooks.length / vm.itemsPerPage);
		}else{
			vm.filteredBooks = vm.list;
			vm.setDefaultTotalPages();
		}
		vm.currentPage = 1;

	});


	return vm;

};
});
