define(['angular',
	'scripts/modules/home/HomeController',
	'scripts/modules/home/offsetFilter'
],function(angular,HomeController,offsetFilter){
	'use strict';
	
	return angular.module('app.home',[])
	  .controller('HomeController',[
	    '$scope',
	    '$log',
	    HomeController])
	  	.filter('offset', offsetFilter);
});