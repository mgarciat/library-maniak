define([],function(){
  'use strict';

  return function() {
		return function(input, start) {
			input = input || [];
			start = parseInt(start, 10);
			return input.slice(start);
		};
	}

});
