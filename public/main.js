require.config({

	baseUrl : '.',

	paths : {
		//Vendor
		'angular' : '../bower_components/angular/angular',
		'ui.router' : 'bower_components/angular-ui-router/release/angular-ui-router',
		'angular-resource' : 'bower_components/angular-resource/angular-resource',
		'uiBootstrap' : 'bower_components/angular-bootstrap/ui-bootstrap-tpls',
		'lodash' : 'bower_components/lodash/lodash',
		//Scripts
		'home' : './scripts/modules/home/index',
		'books' : './scripts/modules/books/index'

	},
	shim : {
		'angular' : {
	      exports : 'angular'
	    },
	    'angular-resource' : {
	      deps : ['angular'],
	    },
	    'ui.router': {
	      deps: ['angular']
	    },
	    'uiBootstrap': {
	      deps: ['angular'],
	      exports: 'uiBootstrap'
	    }
	}

});


require([
	'app',
	'scripts/router.js'
],function(
	app,
	router
){
	'use strict';
	app.config(router);
	angular.bootstrap(document,['app']);

});