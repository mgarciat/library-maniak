define(['ui.router',
'angular-resource',
'uiBootstrap',
'home',
'books'
],function(){
	'use strict';

	var app;

	app = angular.module('app', [
		'ui.router',
		'ngResource',
		'ui.bootstrap',
		'app.home',
		'app.books'
	]);

	return app;

});