'use strict';
var express = require('express'),
    router = express.Router(),
    db = require('../shared/db_manager');

router.get('/get', function (req, res) {
	var query = 'SELECT b.id, b.name, b.author, b.published_date, c.category_name, m.username FROM books b INNER JOIN categories c ON b.category = c.id INNER JOIN members m ON m.id = b.member';
	db.query(query,[],function(err,results){
		res.json({ books: results.rows });
	});
});

router.post('/create', function (req, res) {
	var params = [],
	book = req.body.book;
	params.push(book.name,book.author,book.category.id,book.published_date);
	var query = 'INSERT INTO books(name,author,category,published_date) VALUES ($1,$2,$3,$4)';
	db.query(query,params,function(err,results){
		res.json({ books: results.rows });
	});
});

router.put('/update', function (req, res) {
	var params = [],
	book = req.body.book;
	console.log(book);
	db.query('SELECT * FROM members WHERE username = $1;',[book.username],function(err,results){
		var userId = results.rows[0].id;
		console.log(results);
		console.log(err);
		console.log(userId);
		params.push(book.name, book.author, book.category.id, book.published_date, userId, book.id);
		var query = 'UPDATE books SET name = $1,author = $2,category = $3,published_date = $4, member = $5 WHERE id = $6';
		db.query(query,params,function(err,results){
			res.json({ error : err, books: results.rows });
		});
	});


	
});

router.delete('/delete/:id', function (req, res) {
	var params = [],
	bookId = req.params.id;
	params.push(bookId);
	var query = 'DELETE FROM books WHERE id = $1';
	db.query(query,params,function(err,results){
		if(err){
			res.json(err);
		}else{
			res.json({ success: true });
		}
	});
});

module.exports = router;