'use strict';
var express = require('express'),
    router = express.Router(),
    db = require('../shared/db_manager');

router.get('/get', function (req, res) {
	var query = 'SELECT m.id, m.username FROM members m ORDER BY m.id;';
	db.query(query,[],function(err,results){
		res.json({ members : results.rows });
	});
});

module.exports = router;