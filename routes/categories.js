'use strict';
var express = require('express'),
    router = express.Router(),
    db = require('../shared/db_manager');

router.get('/get', function (req, res) {
	var query = 'SELECT * FROM categories;';
	db.query(query,[],function(err,results){
		res.json({ categories : results.rows });
	});
});

module.exports = router;