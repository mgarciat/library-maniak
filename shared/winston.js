var logLevel = process.env.WINSTON_LOG_LEVEL || 'debug';
var env = process.env.NODE_ENV || 'development';
var prependTimestamp = (env === 'development') ? true : false;

var winston = require('winston');

// see https://github.com/flatiron/winston/issues/206
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, { colorize: true, prettyPrint: true, level: logLevel, timestamp : prependTimestamp });

module.exports = winston;