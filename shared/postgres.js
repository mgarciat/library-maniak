var pg = require('pg'),
winston = require('./winston'),
connString = 'postgres://postgres:postgres@localhost/maniaklib',
postgresdb = {


	_client : null,

	_done : null,

	//the getter for the client
	getClient : function(){
		return postgresdb._client;
	},

	getDone : function(){
		return postgresdb._done;
	},

	initialize : function(cb){
		pg.connect(connString,function(err,client,doneCb){
			if(err){
				winston.error('Error fetching pool client ', err);
			}
			postgresdb._client = client;
			postgresdb._done = doneCb;
			cb();
		});
	},
	closeClient : function(){
		if(postgresdb._done){
			postgresdb._done();
		}else{
			assert(postgresdb._client,'Client should be initalized');
			postgresdb._client.end();
		}
	},

};

module.exports = postgresdb;