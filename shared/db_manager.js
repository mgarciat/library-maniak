'use strict';
var winston = require('./winston'),
assert = require('assert'),
postgres = require('./postgres'),
dbManager = {

	query : function(query,args,callback){
			postgres.initialize(function(){
				postgres.getClient().query(query,args,function(e,result){
                    postgres.closeClient();
					if(e){
                        winston.error('Error processing query :', e);
                        if (callback) {
                            callback(e, result);
                        }
					}else{
						winston.info('Query completed ', query);
						if(callback){
							callback(null, result);
						}
					}
				});
            });
		}


}

module.exports = dbManager;