'use strict';
var gulp = require('gulp'),
nodemon = require('gulp-nodemon'),
browserSync = require('browser-sync').create();


gulp.task('start-server', function () {
  nodemon({
    script: 'index.js'
  , ext: 'js'
  , env: { 
  	'NODE_ENV': 'development'
  	 }
  })
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./public"
        }
    });
});


gulp.task('app', ['browser-sync']);

gulp.task('server',['start-server'])