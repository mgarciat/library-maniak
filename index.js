'use strict';

var express = require('express'),
app = express(),
http = require('http'),
bodyParser = require('body-parser'),
morgan = require('morgan'),
winston = require('./shared/winston'),
path = require('path');




app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(morgan('combined'));


//app.set('view engine', 'html');
//app.set('views',__dirname + '/app');


app.all('*',function(req,res,next){
	//for sake of convenience, accept all requests for now
	res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Credentials', true);
    res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
    res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
    res.set('Cache-Control', 'private, max-age=0, no-cache');

    //cors preflight ok response
    if ('OPTIONS' === req.method) {
        return res.sendStatus(200);
    }

    next();

});


var routes = {};

routes.home = require('./routes/home.js');
routes.books = require('./routes/books.js');
routes.categories = require('./routes/categories.js');
routes.users = require('./routes/users.js');

app.use('/',routes.home);
app.use('/books',routes.books);
app.use('/categories',routes.categories);
app.use('/users',routes.users);

var port = 8888;
var server = http.createServer(app);
server.listen(port, function () {
    winston.info('Library Server is listening on port ' + port);
});
