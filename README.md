#Maniak Library

##Tools of the trade
- AngularJS for client-side stuff.
- NPM / Bower as package managers.
- Express for back-end.
- PostgresSQL as DB.

###Dev Setup
You need to have Node on your machine

	node --version

Also you will need to import the DB Script:
	
	/lib_dump.sql

You can do that with pgAdmin (by default you get it when you download [PostgreSql](https://www.postgresql.org/download/))

There are some npm packages that you should have installed globally and some for the repository, you can get'em with:
	
	npm install gulp bower -g
	npm install


After some minutes(this can be a long task, be patient) when the process finish you can install front-end dependencies with:

	bower install

So now you can bootstrap the 'server' with:

	gulp server

And then you need to run the 'client' app through:

	gulp app

