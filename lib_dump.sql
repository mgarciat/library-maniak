--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-06-21 23:26:01

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2130 (class 1262 OID 52505)
-- Name: maniaklib; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE maniaklib WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';


ALTER DATABASE maniaklib OWNER TO postgres;

\connect maniaklib

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2133 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 52858)
-- Name: books; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE books (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    author character varying(150) NOT NULL,
    category integer,
    published_date timestamp without time zone,
    member integer DEFAULT 1
);


ALTER TABLE books OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 52856)
-- Name: books_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE books_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE books_id_seq OWNER TO postgres;

--
-- TOC entry 2134 (class 0 OID 0)
-- Dependencies: 185
-- Name: books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE books_id_seq OWNED BY books.id;


--
-- TOC entry 182 (class 1259 OID 52839)
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categories (
    id integer NOT NULL,
    category_name character varying(200) NOT NULL,
    description character varying(150) NOT NULL
);


ALTER TABLE categories OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 52837)
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq OWNER TO postgres;

--
-- TOC entry 2135 (class 0 OID 0)
-- Dependencies: 181
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- TOC entry 184 (class 1259 OID 52847)
-- Name: members; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE members (
    id integer NOT NULL,
    username character varying(120) NOT NULL,
    password text NOT NULL
);


ALTER TABLE members OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 52845)
-- Name: members_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE members_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE members_id_seq OWNER TO postgres;

--
-- TOC entry 2136 (class 0 OID 0)
-- Dependencies: 183
-- Name: members_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE members_id_seq OWNED BY members.id;


--
-- TOC entry 1996 (class 2604 OID 52861)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY books ALTER COLUMN id SET DEFAULT nextval('books_id_seq'::regclass);


--
-- TOC entry 1994 (class 2604 OID 52842)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- TOC entry 1995 (class 2604 OID 52850)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY members ALTER COLUMN id SET DEFAULT nextval('members_id_seq'::regclass);


--
-- TOC entry 2125 (class 0 OID 52858)
-- Dependencies: 186
-- Data for Name: books; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO books VALUES (21, 'Woman', 'Charles Bukowski', 1, '2016-08-08 05:00:00', 6);
INSERT INTO books VALUES (1, 'The Little Prince', 'Antoine de Saint-Exupéry', 1, '2016-06-20 14:54:02.136794', 1);
INSERT INTO books VALUES (7, 'Don Quixote', 'Miguel de Cervantes', 3, '1913-06-20 19:54:02.136', 3);
INSERT INTO books VALUES (3, 'The Old Man and The Sea', 'Ernest Hemingway', 1, '2016-06-20 14:54:02.136794', 1);
INSERT INTO books VALUES (13, 'In Search of Lost Time', 'Marcel Proust', 2, '1913-06-20 14:54:02.136794', 2);
INSERT INTO books VALUES (8, 'Moby Dick', 'Herman Melville', 2, '1913-06-20 19:54:02.136', 4);
INSERT INTO books VALUES (20, 'The Old Man and The Sea', 'Ernest Hemingway', 1, '2016-06-21 05:54:02.136', 1);
INSERT INTO books VALUES (9, 'The Lord of The Rings', 'J.R.R. Tolkien', 1, '1930-06-20 19:54:02.136', 4);
INSERT INTO books VALUES (15, '1984', 'George Orwell', 3, '1913-06-20 14:54:02.136794', 4);
INSERT INTO books VALUES (16, 'Perfume', 'Patrick Suskind', 2, '1913-06-20 19:54:02.136', 6);
INSERT INTO books VALUES (4, 'Factotum', 'Charles Bukowski', 1, '2016-06-21 00:54:02.136', 1);
INSERT INTO books VALUES (2, 'The Art of War', 'Sun Tzu', 3, '2016-06-20 19:54:02.136', 3);
INSERT INTO books VALUES (6, 'Ulysses', 'James Joyce', 1, '1933-06-21 15:54:02.136', 3);
INSERT INTO books VALUES (11, 'Song Of Ice And Fire', 'George R.R. Martin', 1, '2016-06-21 05:54:02.136', 6);
INSERT INTO books VALUES (12, 'One Hundred Years of Solitude', 'Gabriel Garcia Marquez', 1, '2016-06-20 19:54:02.136', 6);
INSERT INTO books VALUES (10, 'The Catcher in The Rye', 'J.D. Salinger', 3, '2016-06-21 00:54:02.136', 2);


--
-- TOC entry 2137 (class 0 OID 0)
-- Dependencies: 185
-- Name: books_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('books_id_seq', 21, true);


--
-- TOC entry 2121 (class 0 OID 52839)
-- Dependencies: 182
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO categories VALUES (1, 'Narrative', 'Narrative');
INSERT INTO categories VALUES (2, 'Science Fiction', 'Science Fiction');
INSERT INTO categories VALUES (3, 'Political', 'Political');


--
-- TOC entry 2138 (class 0 OID 0)
-- Dependencies: 181
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categories_id_seq', 2, true);


--
-- TOC entry 2123 (class 0 OID 52847)
-- Dependencies: 184
-- Data for Name: members; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO members VALUES (3, 'adominguez', 'admi123');
INSERT INTO members VALUES (4, 'lrodriguez', 'lrd123');
INSERT INTO members VALUES (5, 'jdavid', 'jdv123');
INSERT INTO members VALUES (1, 'AVAILABLE', 'AVAIL');
INSERT INTO members VALUES (6, 'mgarciat', 'mg3356');
INSERT INTO members VALUES (2, 'UNAVAILABLE', 'UNAVAIL');


--
-- TOC entry 2139 (class 0 OID 0)
-- Dependencies: 183
-- Name: members_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('members_id_seq', 1, false);


--
-- TOC entry 2003 (class 2606 OID 52863)
-- Name: books_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- TOC entry 1999 (class 2606 OID 52844)
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- TOC entry 2001 (class 2606 OID 52855)
-- Name: members_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY members
    ADD CONSTRAINT members_pkey PRIMARY KEY (id);


--
-- TOC entry 2004 (class 2606 OID 52864)
-- Name: books_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_category_fkey FOREIGN KEY (category) REFERENCES categories(id);


--
-- TOC entry 2005 (class 2606 OID 52869)
-- Name: books_member_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_member_fkey FOREIGN KEY (member) REFERENCES members(id);


--
-- TOC entry 2132 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-06-21 23:26:02

--
-- PostgreSQL database dump complete
--

